$(document).ready(function(){

	// Top slider
	$('#top-slider').carousel({
		interval: 4000
	});
	// Promo slider
	$('#promo-slider').carousel({
		interval: 3000
	});
	// Menu mobilne
	mobileMenu();


	//ładowanie warunkowe skryptu skrollr
	if (document.documentElement.clientWidth > 1200) {
		$.getScript('js/skrollr.min.js', function(){
			//skrollr parallax
			var s = skrollr.init({forceHeight: false});
		});
	}

//odliczanie tylko wtedy, kiedy div jest widoczny na ekranie, następnie zakończenie animacji
	$(window).on('scroll', function(event) {
		if($(window).scrollTop() > ($("#statistics").offset().top - (2 * $("#statistics").height()) )) {
			// odliczanie
			$('.timer').countTo();
			$(window).off("scroll");
		}
	});

});

// Scroll dla naglowka
$(window).on('scroll', function(event) {
	if($(window).scrollTop() > 20) {
		$('.main-header').addClass('header-short');
		$('.main-logo img').css('max-height', 28);
	} else {
		$('.main-header').removeClass('header-short');
		$('.main-logo img').css('max-height', 42);
	}
});

// Menu mobilne
function mobileMenu() {
	$('#mobile-icon').on('click', function(event) {
		event.preventDefault();
		$('#main-nav-mobile').show();
	});
	$('#mobile-close').on('click', function(event) {
		event.preventDefault();
		$('#main-nav-mobile').hide();
	});
}
